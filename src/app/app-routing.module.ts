import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},

  //HomePage routes and childrens
  {
    path: 'home',
    children:[
      {
        path: '',
        loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
      },
      {
        path: ':itemID',
        loadChildren: () => import('./home/item-detail/item-detail.module').then( m => m.ItemDetailPageModule)
      },
      {
        path: 'favorites',
        children:[
          {
            path: '',
            loadChildren: () => import('./home/favorites/favorites.module').then(m => m.FavoritesPageModule)
          },
          {
            path: ':itemID',
            loadChildren: () => import('./home/item-detail/item-detail.module').then( m => m.ItemDetailPageModule)

          }
        ]

      }
    ]
  },
  //HomePage routes and childrens

  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },

  //LoginPage routes and childrens
  {
    path: 'login',
    children: [
      {
        path: '',
        loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
      },
      {
        path: 'register',
        loadChildren: () => import ('./login/register/register.module').then( m => m.RegisterPageModule)
      }
    ]
  },
  //LoginPage routes and childrens

  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
