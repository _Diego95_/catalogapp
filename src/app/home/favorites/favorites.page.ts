import { Component, OnInit } from '@angular/core';
import { Item } from '../item.model';
import { HomeService } from '../home.service';




@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {

  favItems: Item[];

  constructor(private homeService: HomeService) { 
  }

  ngOnInit() {
    this.favItems = this.homeService.getAllFavItems();
   
  }

}
