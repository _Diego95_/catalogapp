import { Component, OnInit } from '@angular/core';
import { Item } from './item.model';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  items: Item[];

  constructor(private homeService: HomeService) {}


  ngOnInit(){
    this.items = this.homeService.getAllItems();
  }

  filterList(event: any){
    const itemValue = event.target.value;
    this.items = this.homeService.getAllItems();
    
    if(itemValue && itemValue.trim() != ""){
      this.items = this.items.filter((item) => {
        return (item.title.toLowerCase().indexOf(itemValue.toLowerCase())>-1);
      })
    }
  }

}
