import { Injectable } from '@angular/core';
import { Item } from './item.model';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  favItemsList: Item[]=[];

  private items: Item[] = [
    {
      id: 'i1',
      title: 'Silla_1',
      price: '$10.00',
      description: 'Relax Chair with high quality materials,Good choice for outdoor use',
      imagePath: 'assets/icon/chair_one.jpg',
      isFavorite: false,
    },
    { 
      id: 'i2',
      title: 'Silla_2',
      price: '$15.00',
      description: 'Modern chair with a cool design',
      imagePath: 'assets/icon/chiar_two.jpg',
      isFavorite: false,
    },
    {
      id: 'i3',
      title: 'Sofa_1',
      price: '$15.00',
      description: 'Modern design for family',
      imagePath: 'assets/icon/sofa_one.jpg',
      isFavorite: false
    },
    {
      id: 'i4',
      title: 'Sofa_2',
      price: '$15.00',
      description: 'Modern design for family',
      imagePath: 'assets/icon/sofa_two.jpg',
      isFavorite: false
    }
    
  ];


  constructor() { }


  //Item methods
  getAllItems(){
    return [...this.items];
  }
  getItem(recipeId: string){
    return this.items.find(item =>{
      return item.id === recipeId;
    })
  }
  //Item methods

  //Fav Item Method
  getFavItem(recipeId: string){
    return this.favItemsList.find(item =>{
      return item.id === recipeId;
    })
  }
  
  getAllFavItems(){
    return [...this.favItemsList];
  }

  removeFromFavList( recipeId: string){

    const index: number = this.favItemsList.indexOf(this.getFavItem(recipeId));
    if(index !== -1) {
      this.favItemsList.splice(index, 1);
    }

  }
  //Fav Item Method
}
