import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HomeService } from '../home.service';
import { HomePage } from '../home.page';
import { Item } from '../item.model';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.page.html',
  styleUrls: ['./item-detail.page.scss'],
})
export class ItemDetailPage implements OnInit {
  loadedItem: Item;
  isFavorite: boolean = false;
  



  constructor(private activatedRoute: ActivatedRoute, private homeService: HomeService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap =>{
      if(!paramMap.has('itemID')) {
        return;
      }
      const itemId = paramMap.get('itemID');
      this.loadedItem = this.homeService.getItem(itemId);
      
    });
  }



  addToFavorites(){

    this.activatedRoute.paramMap.subscribe(paramMap => {
      const favItem = paramMap.get('itemID');
      const isFav = this.homeService.getItem(favItem).isFavorite;
    
      if(isFav === false){
        this.homeService.getItem(favItem).isFavorite = true;
        this.homeService.favItemsList.push(this.homeService.getItem(favItem));
        
      }else {
        this.homeService.getItem(favItem).isFavorite = false;
        this.homeService.removeFromFavList(favItem);
      }
    })
  }


}
