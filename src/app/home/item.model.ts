export interface Item {
    id: string,
    title: string,
    price: string,
    description: string,
    imagePath: string,
    isFavorite: boolean
}